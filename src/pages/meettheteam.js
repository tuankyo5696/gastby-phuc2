import React from "react"
//import {Link} from "gatsby"
import Layout from './../components/layout'
import Head from './../components/head'

const IndexPage =()=>{
  return(
      <Layout>
            <Head title="Meet the team"/>
            <h1>Meet the team</h1>
      </Layout>
    
  )
}

export default IndexPage
