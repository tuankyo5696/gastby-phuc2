import React from "react"
import {Link} from "gatsby"
import Layout from './../components/layout'
import Head from './../components/head'

const IndexPage =()=>{
  return(
      <div>
            <Layout>
            <Head title="Private Policy"/>
            <h1>Hello Gatby JS</h1>
            <h2>I'm Phuc, a full stack developer</h2>
            <p>Need a developer?<Link to="/contactus">Let's Contact me</Link></p>
            </Layout>
      </div>
    
  )
}

export default IndexPage
