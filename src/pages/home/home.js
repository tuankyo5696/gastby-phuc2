import React, { Component } from 'react'

import './home.scss'

import Partner from "./partnerships/partner";
import Difference from "./difference/difference";
import Approach from "./approach/approach";
import Services from "./services/services";
import Clients from "./clients/clients";
import HomeTitle from "./homeTitle/homeTitle";
import ClientsList from "./clientsList/clientsList";
//import {Link} from "gatsby"

class IndexPage extends Component{
    state = {
        scroll : false,
        scrolling1: false,
        scrolling2: false,
        scrolling3: false,
        scrolling4: false
    }
    componentDidMount() {
        window.addEventListener('scroll',this.handleScroll)
        window.addEventListener('scroll', this.handleScroll)
        window.addEventListener('scroll', this.handleScroll1)
        window.addEventListener('scroll', this.handleScroll2)
        window.addEventListener('scroll',this.handleScroll3)
    }
    handleScroll = (e) => {
        window.scrollY === 0 ? this.setState({scroll: false}) : this.setState({scroll: true})
        window.scrollY > 600 && window.scrollY < 800 ? this.setState({ scrolling1: true }) : this.setState({ scrolling1: false })
        console.log(window.scrollY)
    }
    handleScroll1 = (e) => {
        window.scrollY > 800 && window.scrollY < 1500 ? this.setState({ scrolling2: true }) : this.setState({ scrolling2: false })
    }
    handleScroll2 = (e) => {
        window.scrollY > 1600 && window.scrollY < 2300? this.setState({ scrolling3: true }) : this.setState({ scrolling3: false })
    }
    handleScroll3 = (e) => {
        window.scrollY > 2100 ? this.setState({ scrolling4: true }) : this.setState({ scrolling4: false })
    }
    componentWillUnmount(){
        window.removeEventListener('scroll', this.handleScroll);
        window.removeEventListener('scroll', this.handleScroll1);
        window.removeEventListener('scroll', this.handleScroll2);
        window.removeEventListener('scroll', this.handleScroll3);
      }
    render() {
        return (   
            <div>
                  <div className="container-fluid body" >
                     <HomeTitle/>
                  <hr className="white"/>
                        <ClientsList/>
                    <Clients scroll={this.state.scrolling1}/>
                  <hr className="shadow up"/>
                    <Services scroll={this.state.scrolling2}/>
                  <hr className="shadow"/>
                    <Approach scroll={this.state.scrolling3}/>
                  <hr className="shadow up"/>
                    <Difference scroll={this.state.scrolling4}/>
                  <hr className="shadow"/>
                      <Partner />
              </div>
      
      
              </div>
        )
    }
}

export default IndexPage
