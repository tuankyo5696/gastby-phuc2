import React from "react"
import "./clients.scss"


import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";

const Clients = (props) => {
    let attachClasses = ["animate default right", ""]
    if (props.scroll) {
        attachClasses = ["animate default right", "in-view"]
    }
    const opptions={
        infinite:true,
        buttonsDisabled:true,
        dotsDisabled:true,
        autoPlay:true,
        autoPlayInterval:2000,
        onResized:{
            item: 1,   // index of the current item`s position
            slide: 1,   // index of the current slide`s position
            itemsInSlide: 1   // number of elements in the slide
        },
        responsive:{
            0: {
                items: 1
            },
            1024: {
                items: 1
            }   // number of elements in the slide
        },
        
        
    }

  return(
    <div>
        <div className="clients" style={{minHeight: '219px'}}>

            <div className="align-middle">
                <div className="ttaPadding">
                    <div className="quote animate">
                        <div id="alert-success">
                            <AliceCarousel AutoPlaymode {...opptions} >
                                <div className="testimonial">
                                    <p><span>Everyone has had such positive feedback about the smoothness 
                                        and professionalism of how it all went, which is a testament to all
                                            your hard work over many months.</span></p>
                                    <p className="attribution"><span>Clients – International Legal Firm</span></p>
                                </div>
        
                                <div className="testimonial">
                                    <p><span>Our event was great success thanks to the amazing team we had onsite,
                                            who not only deliver but go the extra mile to exceed everybody's expectations.</span></p>
                                    <p className="attribution"><span>Global Congress &amp; Engagement Strategy Lead, Healthcare</span></p>
                                </div>

                                <div className="testimonial">
                                    <p><span>Thanks for all your dedication, expertise and professionalism 
                                        in bringing the meeting to life once again.</span></p>
                                    <p className="attribution"><span>Healthcare Event Stakeholder</span></p>
                                </div>

                            </AliceCarousel>
                        </div>  
                        
{/* 
                        <div className="testimonial">
                            <p><span>This really is a high-stakes event for the team and you made 
                                a very challenging event look easy to execute.</span></p>
                            <p className="attribution"><span>Healthcare Event Stakeholder</span></p>
                        </div>

                        <div className="testimonial">
                            <p><span>There are so many complexities with an event like this – 
                                thanks to such an expert team it was flawlessly delivered.</span></p>
                            <p className="attribution">
                                <span>Healthcare Event Stakeholder</span></p>
                        </div>

                        
                        <div className="testimonial">
                            <p><span>I am really delighted with how the meeting went, and just 
                                wanted to thank you all for your energy&nbsp;and commitment – 
                                looking forward to working with you again!</span></p>
                            <p className="attribution"><span>Franchise Leader, Healthcare Company</span></p>
                        </div>

                        <div className="testimonial">
                            <p><span>We've had great feedback – 85% said the day was ‘fantastic’ 
                                (15% said ‘good’) – you were a massive part of that.</span></p>
                            <p className="attribution"><span>UK Charity Trustee</span></p>
                        </div>

                        <div className="testimonial">
                            <p><span>I'm not sure I ever really knew what an events management 
                                team could do before we met you, and you've all opened my eyes
                                    to how important your work is.</span></p>
                            <p className="attribution"><span>UK Charity Trustee</span></p>
                        </div>

                        <div className="testimonial">
                            <p><span>I have found Medi Square to be an outstanding agency – they make 
                                the process effortless, understand the pharmaceutical industry 
                                very well and have a solution-focused approach to situations.</span></p>
                            <p className="attribution"><span>Healthcare Medical Advisor</span></p>
                        </div>

                        <div className="testimonial">
                            <p><span>The whole team was absolutely exceptional: excellent client 
                                service, responsiveness and delivery from start to finish.</span></p>
                            <p className="attribution"><span>Pharma Communications Director</span></p>
                        </div> */}

                    
                    


                    </div>
                    <div className="smileIcon">
                        <div className="divIcon">
                            <div className="cover divdraw">
                                <div className="bottom divdraw"></div>
                            </div>
                            <div className="tamgiac">
                                <svg height="50" width="50">
                                    <polyline points="0 0, 0 50, 50 0, 0 50" style={{fill:'white',stroke:'black',strokeWidth:'1'}} />
                                </svg>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div> 


            <div className="clientsDiv">
                <div className="ttaPadding">
                    <div className={attachClasses.join(" ")}>
                        <h2>Our <strong>clients</strong></h2>
                        <p>
                            We work with forward-thinking global organisations 
                            across the healthcare, corporate and non-profit sectors to change
                                minds and lives through immersive events and experiences. 
                            <br/>
                            <br/>
                            These programmes bring people together to expand their knowledge, 
                            solve problems, share ideas and identities, be inspired and invigorated,
                                and make lasting memories and connections. 
                            <br/>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}


export default Clients