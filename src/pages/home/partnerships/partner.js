import React from "react"
import "./partner.scss"

import { OverPack } from 'rc-scroll-anim';
import TweenOne from 'rc-tween-one';
import QueueAnim from 'rc-queue-anim';


const Partner =()=>{
  return(
    <div>
        <div className="partnerships2 container">
          <div className="ttaPadding">
            <div className="animate default right">
              <h2>Our <strong>partnerships</strong></h2>
              <p>We cultivate and celebrate relationships that are meaningful 
                  rather than simply transactional. So whether your event is in Preston, 
                  Paris or Pretoria, our specialist, long-term partnerships with outstanding 
                  international suppliers enable us to secure the best value for you, 
                  with full confidence in quality and flawless execution.   
                  <br/>
              </p>
            </div>
          </div>
          <div className="divPartner">

          </div>
        </div>


        <OverPack style={{ overflow: 'hidden', height: '200px' }} >
          <TweenOne key="0" animation={{ opacity: 1 }}
            className="code-box-shape"
            style={{ opacity: '0', marginBottom: '10' }}
          />
          <QueueAnim key="queue"
            leaveReverse
            style={{ float: 'left', position: 'relative', left: '50%', marginLeft: '-165px', height:'100px' }}
          >
            <div key="a" className="code-box-shape queue-anim-demo" />
            <div key="b" className="code-box-shape queue-anim-demo" />
            <div key="c" className="code-box-shape queue-anim-demo" />
            <div key="d" className="code-box-shape queue-anim-demo" />
            <div key="e" className="code-box-shape queue-anim-demo" />
            <div key="f" className="code-box-shape queue-anim-demo" />
          </QueueAnim>
        </OverPack>


    </div>
  )
}


export default Partner