import React, { Component } from 'react'

//import {Link} from "gatsby"
import Layout from './../components/layout'
import Head from './../components/head'
import Home from './../pages/home/home'

class IndexPage extends Component{
 
  render() {
    return(
      <div>
            <Layout>
            <Head title="Home"/>
            <Home/>
            </Layout>
      </div>
    
  )
  }

}

export default IndexPage

