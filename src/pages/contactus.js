import React from "react"
import Layout from "../components/layout";
import Head from './../components/head'

const ContactPage =()=>{
  return(
    <Layout>
      <Head title="Contact"/>
      <h1>Contact</h1>
      <h2>The best way to reach me: </h2>
      <p>MediSquare</p>
    </Layout>
  )
}


export default ContactPage