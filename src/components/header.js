import React from "react"
import { Link} from "gatsby"
import './header.scss'
import logo from "./../assets/images/logo-final.svg"

import { Menu } from 'react-feather';


class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            toggleClass:''
        };
    }

    addResponsiveClass = () => {
        const currentState = this.state.active;
        this.setState({ active: !currentState, toggleClass: this.state.toggleClass === 'responsive' ? '' : 'responsive' });


    }
    render (){
        const { state: { toggleClass } } = this;
        let attachClasses = ["header", ""];
        if (this.props.scroll) {
            attachClasses = ["header","scrolled"]
        }
        return(
            <header className={attachClasses.join(" ")}>
                <div>
                    <Link to="/">
                        <img src={logo} alt="Logo" className="logo" />
                    </Link>
                    
                </div>
                <div>
                    <nav>
                        <div className={`top-nav ${toggleClass}`}>
                            <div>
                                <Link  className={`nav-item ${toggleClass}`} activeClassName="active-nav-item" to="/">The MEDISQUARE Experience</Link>
                            </div>
                            <div>
                                <Link  className={`nav-item ${toggleClass}`} activeClassName="active-nav-item" to="/ourwork">Our Work</Link>
                            </div>
                            <div>
                                <Link  className={`nav-item ${toggleClass}`} activeClassName="active-nav-item" to="/meettheteam">Meet the team</Link>
                            </div>
                            <div>
                                <Link  className={`nav-item ${toggleClass}`} activeClassName="active-nav-item" to="/whatsnew">What's new?</Link>
                            </div>
                            <div>
                                <Link className={`nav-item ${toggleClass}`} activeClassName="active-nav-item" to="/contactus">Get in touch</Link>
                            </div>
                            <div className={`nav-item-icon ${toggleClass}`} onClick={this.addResponsiveClass}>
                                <Menu />
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
          )
    }

}


export default Header