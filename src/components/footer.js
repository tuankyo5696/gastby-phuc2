import React from "react"
import { Link } from "gatsby"

import twitter from './../assets/images/twitter.png'
import linkedin from './../assets/images/linkedin.png'
import facebook from './../assets/images/facebook.png'
import instagram from './../assets/images/instagram.png'
//import { graphql, useStaticQuery } from "gatsby"
import './footer.scss'

const Footer =()=>{

  return(

    <footer className="footer">

      <p className="p1">
        <span className="s1">Follow us on:</span>
      </p>
      <div className="socialLinks">
        <div className="p1">
          <a title="Follow us on twitter" rel="noopener" href="#/" target="_blank">
            <img src={twitter} alt="Logo" className="imgBottom" />
          </a>
        </div>

        <div className="p1">
          <a title="Join us on LinkedIn" rel="noopener" href="#/" target="_blank">
            <img src={linkedin} alt="Logo" className="imgBottom" />
          </a>
        </div>

        <div className="p1">
          <a title="Like us on Facebook" href="https://www.facebook.com/medisquare8/" rel="noopener noreferrer" target="_blank">
            <img src={facebook} alt="Logo" className="imgBottom"/>
          </a>
          
        </div>
        <div className="p1">
          <a title="Follow us on Instagram" href="https://www.instagram.com/medisquarehospital/" rel="noopener noreferrer" target="_blank">
            <img src={instagram} alt="Logo" className="imgBottom instagram" />
          </a>
          
        </div>
      </div>

      <div className="menu">
        <div className="link">
          <Link className="page" to="/contactus" title="Contact Us">Contact Us</Link>
        </div>

        <div className="link">
          <Link className="page" to="/privatepolicy" title="Your privacy matters to us">Your privacy matters to us</Link>
        </div>

        <div className="link">
          <Link className="page" to="/qualitypolicy" title="Our quality statement">Our quality statement</Link>
        </div>

      </div>
      <p className="copyright">© Copyright 2018 The Turner Agency</p>

  </footer>

  )
}


export default Footer