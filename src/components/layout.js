import React, { Component } from 'react'

import Header from "./../components/header"
import Footer from "./../components/footer"
import './../styles/index.scss'
//import LayoutStyles from './layout.module.scss'

class Layout extends Component{
  state = {
    scroll: false
  }
  componentDidMount() {
    window.addEventListener('scroll',this.handleScroll)
  }
  handleScroll = (e) => {
    window.scrollY === 0  ? this.setState({scroll : false}) : this.setState({scroll: true})
  }
  render() {
    return(
      <div style={{backgroundColor: '#E9E8E9'}}>
        <div>
          <Header scroll={this.state.scroll}/>
          {this.props.children}
        </div>
        <Footer/>
      </div>
    )
  }
 
}


export default Layout